HOW TO RUN:

1. (Download) Copy PoeChatFilter.exe and config.txt in same folder.
2. Change Path in config.txt to path to your Path of Exile Client.txt
(PoE nstall Folder -> logs -> Client.txt)
3. Start exe while Poe is running and trade/global chat is enabled.

Only messages from trade or global chat will be shown.
Repeated Messages from the same player will be ignored and latest message
apperance logged. You can enter any text in the top textbox to highlight
messages where that text appears. (text must be more then 3 characters)
Multiple keywords can be added by using whitespaces.

Using your Mousewheel will disable Autoscroll.
Clear Button will remove all messages.

Double Click on any Row will copy the whisper to that player in your clipboard.

The changing number is the amount of new bytes found in your Client.txt since
last check.

MIGHT CRASH IF STARTED BEFORE POE IS RUNNING AND CHAT IS VISIBLE!
Start after you are ingame and character is loaded. (have to fix this issue)