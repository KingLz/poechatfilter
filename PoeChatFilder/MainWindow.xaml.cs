﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using PoeChatFilter;

public class StringMessage : INotifyPropertyChanged
{
    public event PropertyChangedEventHandler PropertyChanged;

    public void NotifyPropertyChanged(string propName)
    {
        if (this.PropertyChanged != null)
            this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
    }

    public string Message { get; set; }


}

public class SingleMessage : INotifyPropertyChanged
{
    public event PropertyChangedEventHandler PropertyChanged;

    public void NotifyPropertyChanged(string propName)
		{
			if(this.PropertyChanged != null)
				this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
		}
    public String TimeStamp { get; set; }

    public String LastAppearence { get; set; }
    public string Player { get; set; }

    public string Message { get; set; }

    public bool FilterHighlight { get; set; }

    public bool IsSameMessage(SingleMessage sm)
    {
        return this.Player == sm.Player && this.Message == sm.Message;
    }

    public void ApplyFilteHighlight(List<String> filterList)
    {
        foreach(String f in filterList)
        {
            this.FilterHighlight = this.Message.ToLower().Contains(f);
            if (this.FilterHighlight)
            {
                break;
            }
        }
        this.NotifyPropertyChanged("FilterHighlight");
    }

    public void ClearFilterHighlight()
    {
        this.FilterHighlight = false;
        this.NotifyPropertyChanged("FilterHighlight");
    }

    public void UpdateLastAppearance(String lastAppearance)
    {
        this.LastAppearence = lastAppearance;
        this.NotifyPropertyChanged("LastAppearence");
    }

}

namespace PoeChatFilder
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private System.Windows.Threading.DispatcherTimer UpdateFileInfoTimer;
        private String PoeLogFile;
        private long[] lastPoeLogFileSize;
        private List<String> textfilters = new List<String>();
        private ObservableCollection<SingleMessage> messageCollection = new ObservableCollection<SingleMessage>();
        private ObservableCollection<StringMessage> debugMessageCollection = new ObservableCollection<StringMessage>();
        private String configName = @"config.txt";
        private bool autoscrolling = true;
        private Window1 debugWindow;

        public MainWindow()
        {
            InitializeComponent();
            this.debugWindow = new Window1(this.debugMessageCollection);
            //this.debugWindow.Show();

            if (this.readConfig())
            {
                this.Title = this.PoeLogFile;
                long clientfilesize = new System.IO.FileInfo(this.PoeLogFile).Length;
                this.lastPoeLogFileSize = new long[] { clientfilesize, clientfilesize, clientfilesize, clientfilesize, clientfilesize, clientfilesize, clientfilesize, clientfilesize, clientfilesize, clientfilesize } ;
                this.UpdateFileInfoTimer = new System.Windows.Threading.DispatcherTimer();
                this.UpdateFileInfoTimer.Interval = TimeSpan.FromSeconds(1);
                this.UpdateFileInfoTimer.Tick += this.UpdateFileInfo;
                this.UpdateFileInfoTimer.Start();
                this.listview.ItemsSource = this.messageCollection;
                this.autoscrolling = true;
                this.AutoScrollButton.IsEnabled = false;
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("Config.txt not found. Change config.txt!",
                                          "Error",
                                          MessageBoxButton.OK,
                                          MessageBoxImage.Question);
                Application.Current.Shutdown();
            }

            
        }

        private bool readConfig()
        {
            bool result = false;
            if (File.Exists(this.configName))
            {
                string[] lines = System.IO.File.ReadAllLines(this.configName);
                if (lines.Length > 0)
                {
                    this.PoeLogFile = lines[0];
                    result = File.Exists(this.PoeLogFile);
                }
            }
            
            

            return result;
        }

        private void AddNewListItem(List<SingleMessage> newMessagesList)
        {
           
            bool sameMessage = false;
            foreach (SingleMessage sm in newMessagesList)
            {
                sameMessage = false;
                foreach (SingleMessage lvsm in this.messageCollection)
                {
                    sameMessage = sm.IsSameMessage(lvsm);
                    if (sameMessage)
                    {
                        lvsm.UpdateLastAppearance(sm.LastAppearence);
                        break;
                    }
                }
                if (!sameMessage)
                {
                    if (this.textfilters.Count > 0)
                    {
                        sm.ApplyFilteHighlight(this.textfilters);
                    }
                    this.messageCollection.Add(sm);
                    
                }
            }
            if (this.autoscrolling && this.messageCollection.Count > 5)
            {
                this.listview.ScrollIntoView(this.listview.Items[this.listview.Items.Count - 1]);
            }
            
        }

        private void UpdateFileInfo(object source, EventArgs e)
        {
            long newFileSize = new System.IO.FileInfo(this.PoeLogFile).Length;
            int multibytesToRead = (int)(newFileSize - this.lastPoeLogFileSize[this.lastPoeLogFileSize.Length - 1]);
            if (multibytesToRead > 0)
            {
                String newMessage = this.ReadPoeLogLines(this.lastPoeLogFileSize[this.lastPoeLogFileSize.Length - 1],multibytesToRead);
                if (newMessage.Length > 0)
                {
                    for (int i = 0; i < this.lastPoeLogFileSize.Length -1; i++)
                    {
                        this.lastPoeLogFileSize[this.lastPoeLogFileSize.Length - i - 1] = this.lastPoeLogFileSize[this.lastPoeLogFileSize.Length - i - 2];
                    }
                    this.lastPoeLogFileSize[0] = newFileSize;
                    this.AddNewListItem(this.ProcessPoeLogMessages(newMessage));
                }
                //int lastbytes = (int)(this.lastPoeLogFileSize[0] - this.lastPoeLogFileSize[1]);
                //if (false && lastbytes > 0)
                //{
                //    String lastm = this.ReadPoeLogLines(lastbytes);
                //    if (this.debugWindow.ShowActivated)
                //    {
                //        using (System.IO.StringReader reader = new System.IO.StringReader(lastm))
                //        {
                //            string line = reader.ReadLine();
                //            StringMessage sm = new StringMessage();
                //            sm.Message = line;
                //            this.debugMessageCollection.Add(sm);
                //            while (this.debugMessageCollection.Count > 100)
                //            {
                //                this.debugMessageCollection.RemoveAt(0);
                //            }
                //        }
                //    }
                //}
            }
        }

        private List<SingleMessage> ProcessPoeLogMessages(String newMessages)
        {
            List<SingleMessage> newMessagesList = new List<SingleMessage>();

            using (System.IO.StringReader reader = new System.IO.StringReader(newMessages))
            {
                String line = reader.ReadLine();
                if (!String.IsNullOrEmpty(line))
                {
                    String messagePattern = @".*\[INFO.*?[$|#]";
                    Match messageMatch = Regex.Match(line, messagePattern, RegexOptions.IgnoreCase);
                    if (messageMatch.Success)
                    {
                        String message = Regex.Replace(line, messagePattern, String.Empty);
                        String dateTime = line.Substring(11, 8);
                        String[] playerMessage = message.Split(':');
                        String playerName = playerMessage[0];
                        String tagpattern = @".*<.*>";
                        playerName = Regex.Replace(playerName, tagpattern, String.Empty).Trim();
                        String singleMessage = playerMessage[1].Trim();
                        newMessagesList.Add(new SingleMessage { TimeStamp = dateTime, LastAppearence = dateTime, Player = playerName, Message = singleMessage, FilterHighlight  = false});


                    }



                }

            }
            return newMessagesList;
        }

        private String ReadPoeLogLines(long start, int amount)
        {
            String newMessages = "";
            try
            {

                using (FileStream fs = File.Open(this.PoeLogFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {

                    fs.Seek(start, SeekOrigin.Begin);

                    byte[] bytes = new byte[amount];
                    fs.Read(bytes, 0, amount);

                    newMessages = Encoding.UTF8.GetString(bytes);
                }                
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }
            return newMessages;
        }

        private void Poepathpicker_Click()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                Console.WriteLine(openFileDialog.FileName);
            }
        }

        private void Listview_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = this.listview.SelectedItem as SingleMessage;
            if (item != null)
            {
                String whisper = "@" + item.Player;
                Clipboard.SetText(whisper);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            this.messageCollection.Clear();
        }

        private void TextFilterBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            String[] filterParts = this.TextFilterBox.Text.ToLower().Split(' ');
            List<String> filters = new List<String>();
            foreach(String f in filterParts)
            {
                if(f.Length > 2)
                {
                    filters.Add(f);
                }
            }
            this.textfilters = filters;
            if (this.textfilters.Count > 0)
            {
                this.ApplyFilterToAllItems();
            }
            else
            {
                this.ClearFilterBackground();
            }

        }

        private void ClearFilterBackground()
        {
            foreach (SingleMessage sm in this.messageCollection)
            {
                sm.ClearFilterHighlight();
            }
        }

        private void ApplyFilterToAllItems()
        {
            foreach(SingleMessage sm in this.messageCollection)
            {
                sm.ApplyFilteHighlight(this.textfilters);
            }
        }

        private void AutoScroll_Click(object sender, RoutedEventArgs e)
        {
            this.autoscrolling = true;
            this.AutoScrollButton.IsEnabled = false;
        }


        private void Window_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            this.autoscrolling = false;
            this.AutoScrollButton.IsEnabled = true;
        }
    }
}
